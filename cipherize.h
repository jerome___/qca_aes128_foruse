#ifndef CIPHERIZE_H
#define CIPHERIZE_H

#include <QtCrypto>
#include <QDebug>
#include <QtXml>
#include <QDomDocument>
#include <QDomNode>
#include <QFile>
#include <QFileInfo>

using namespace QCA;

class Cipherize {
public:
    Cipherize();
    ~Cipherize();
protected:
    SymmetricKey         key;
    InitializationVector iv;
	Initializer          init;
    QByteArray           keyba, ivba;
    QString              encoded, decoded;
    QFile               *file;
    QDomDocument        *doc;
    QString              secret, fileName;
	QString cipherize(const QString &word,
	                     const Direction &direction);
    void          cipherOfficialExampleTest(const QString &text);
    bool          openFile(QFile *f);
    bool          truncateFile(QFile *f);
    void          recordStremXML();
    void          loadXMLfile();
    void          initXMLfile();
    QString       getCipherXML();
    bool          setCipherXML(const QString &cipher = "");
};

#endif // CIPHERIZE_H
