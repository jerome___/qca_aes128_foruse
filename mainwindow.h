#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>
#include <QTextEdit>
#include <QPushButton>
#include <QGroupBox>
#include <QCheckBox>
#include <QLineEdit>
#include "cipherize.h"

namespace Ui { class MainWindow; }
using namespace QCA;

class MainWindow : public QMainWindow, public Cipherize {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_checkBox_toggled(bool checked);
    void on_keyPassword_editingFinished();
    void on_buttonNewFile_clicked();
    void on_buttonResetXmlFile_clicked();
    void on_file_textChanged(const QString &arg1);
	void on_toEncode_textChanged();
    void on_toDecode_textChanged();
    void on_encoded_textChanged();
    void on_buttonDefineFile_clicked();
    void on_buttonQCA_clicked();
    void on_buttonSaveFile_clicked();
    void on_buttonCopieToDecode_clicked();
    void on_buttonOpenFile_clicked();
    void on_buttonDecode_clicked();
    void on_buttonEraseAll_clicked();
    void on_actionQuitter_triggered();
    void on_buttonEraseEncode_clicked();
    void on_buttonEraseDecode_clicked();

private:
    Ui::MainWindow      *ui;
    bool                 file_defined;
};

#endif // MAINWINDOW_H
