#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {
	ui->setupUi(this);
    file_defined = false;
    ui->groupBox_decode->setEnabled(false);
    ui->groupBox_encode->setEnabled(false);
    ui->encoded->setEnabled(false);
    ui->buttonOpenFile->setEnabled(false);
    ui->buttonSaveFile->setEnabled(false);
    ui->buttonQCA->setEnabled(false);
    ui->buttonDecode->setEnabled(false);
    ui->buttonDefineFile->setEnabled(false);
    ui->checkBox->setChecked(false);
    ui->buttonNewFile->setEnabled(false);
	ui->buttonResetXmlFile->setEnabled(false);
}

MainWindow::~MainWindow() {
    delete ui;
}

//*********  EVENTS on ENTRIES / LINES EDITED *********************

void MainWindow::on_keyPassword_editingFinished() {
	if(!ui->keyPassword->text().isEmpty()) {
        secret = ui->keyPassword->text();
        key = keyba.append(secret.toUtf8());
        iv  = ivba.append(secret.toUtf8());
        ui->groupBox_decode->setEnabled(true);
        ui->groupBox_encode->setEnabled(true); }
    else {
        ui->groupBox_decode->setEnabled(false);
        ui->groupBox_encode->setEnabled(false); }
}

void MainWindow::on_checkBox_toggled(bool checked) {
    file_defined = checked && !ui->file->text().isEmpty();
    ui->buttonDefineFile->setEnabled(checked);
    ui->buttonNewFile->setEnabled(checked);
    ui->buttonResetXmlFile->setEnabled(checked);
    ui->file->setEnabled(checked);
    ui->buttonOpenFile->setEnabled(file_defined);
    ui->buttonSaveFile->setEnabled(file_defined &&
                                   !ui->encoded->toPlainText().isEmpty());
}

void MainWindow::on_file_textChanged(const QString &arg1) {
    file_defined = !arg1.isEmpty() && ui->checkBox->isChecked();
    ui->buttonOpenFile->setEnabled(file_defined);
    ui->buttonSaveFile->setEnabled(file_defined &&
                                   !ui->encoded->toPlainText().isEmpty());
    if(file_defined) {
        fileName = ui->file->text();
        file = new QFile(fileName); }
}

void MainWindow::on_toEncode_textChanged() {
	ui->buttonQCA->setEnabled(!ui->toEncode->toPlainText().isEmpty());
	encoded = cipherize(ui->toEncode->toPlainText(), Encode);
    ui->encoded->setText(encoded);
}

void MainWindow::on_encoded_textChanged() {
    QString arg1 = ui->encoded->toPlainText();
    ui->buttonSaveFile->setEnabled(!arg1.isEmpty() && file_defined);
}

void MainWindow::on_toDecode_textChanged() {
    QString arg1 = ui->toDecode->toPlainText();
    ui->buttonDecode->setEnabled(!arg1.isEmpty());
    ui->buttonOpenFile->setEnabled(file_defined);
}

//*********  BUTTONS ACTIONS  ************************************

void MainWindow::on_buttonQCA_clicked() {
	cipherOfficialExampleTest(ui->toEncode->toPlainText());
}

void MainWindow::on_buttonCopieToDecode_clicked() {
    ui->toDecode->setText(encoded);
}

void MainWindow::on_buttonDecode_clicked() {
    decoded = cipherize(ui->toDecode->toPlainText(), Decode);
    ui->decoded->setText(decoded);
}

void MainWindow::on_buttonDefineFile_clicked() {
    bool ok(false);
    QString copy_fileName;
    while(copy_fileName.isEmpty() || !ok) {
        copy_fileName = QFileDialog::getOpenFileName( this,
                                       "Open or create XML file",
                                       QDir::homePath(),
                                       QString("XML files (*.xml);;"
                                               "All files (*.*)") );
        if(!copy_fileName.isEmpty()) {
            QFileInfo ifn(copy_fileName);
            ok = ifn.completeSuffix() == "xml";
            if (!ok)
                QMessageBox::critical(this, "Be carrefull",
                                         "The file has to be an xml "
                                         "file with conventionnal extention only !\n"
                                         "While you choose a non xml file, you will "
                                         "not be able to use it, then the soft will refuse it "
                                         "and ask again for a xml file to use "
                                         "(for open or save cipher).");
            else {
                ui->file->setText(copy_fileName);
                loadXMLfile(); } }
        else
            break; }
}

void MainWindow::on_buttonNewFile_clicked() {
    QFileDialog defineFile(this);
    defineFile.setFileMode(QFileDialog::AnyFile);
    defineFile.setNameFilter(QString("XML files (*.xml);;All files (*.*)"));
    defineFile.setViewMode(QFileDialog::Detail);
    defineFile.setDirectory(QDir::homePath());
    defineFile.setAcceptMode(QFileDialog::AcceptSave);
    if(defineFile.exec())
        fileName = defineFile.selectedFiles().at(0);
    if(!fileName.isEmpty()) {
        QFileInfo ifn(fileName);
        QString ffn = ifn.absolutePath() + "/" + ifn.baseName() + ".xml";
        ui->file->setText(ffn);
        loadXMLfile(); }
}

void MainWindow::on_buttonResetXmlFile_clicked() {
    ui->file->clear();
    file_defined = false;
}

void MainWindow::on_buttonSaveFile_clicked() {
    setCipherXML(ui->encoded->toPlainText());
}

void MainWindow::on_buttonOpenFile_clicked() {
    QString result = getCipherXML();
    ui->toDecode->setText(result);
}

void MainWindow::on_buttonEraseAll_clicked() {
   decoded.clear(); encoded.clear();
   ui->encoded->clear();
   ui->decoded->clear();
   ui->toEncode->clear();
   ui->toDecode->clear();
}

void MainWindow::on_buttonEraseEncode_clicked() {
    encoded.clear();
    ui->encoded->clear();
    ui->toEncode->clear();
}

void MainWindow::on_buttonEraseDecode_clicked() {
    decoded.clear();
    ui->decoded->clear();
    ui->toDecode->clear();
}

void MainWindow::on_actionQuitter_triggered() {
    this->close();
}
