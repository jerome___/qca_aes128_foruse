#-------------------------------------------------
#
# Project created by QtCreator 2015-12-20T11:54:35
#
#-------------------------------------------------

QT       += core gui xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qca_aes128_forUse
TEMPLATE = app
CONFIG += c++11 crypto
LIBS += -lqca-qt5

# Disable automatic casts
#DEFINES *= \
#    QT_NO_CAST_TO_ASCII \
#    QT_NO_CAST_FROM_ASCII \
#    QT_NO_CAST_FROM_BYTEARRAY \
#    QT_NO_URL_CAST_FROM_STRING \
#    QT_STRICT_ITERATORS

SOURCES += main.cpp\
        mainwindow.cpp \
    cipherize.cpp

HEADERS  += mainwindow.h \
    cipherize.h

FORMS    += mainwindow.ui
