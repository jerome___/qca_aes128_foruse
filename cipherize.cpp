#include "cipherize.h"

Cipherize::Cipherize() {
	if(!QCA::isSupported("aes256-cbc-pkcs7"))
        qDebug() << "AES-256 not supported";
	secret.clear();
}

Cipherize::~Cipherize() {

}



//*****  CIPHER AES-256, avec CBC et PKCS7  *********************

QString Cipherize::cipherize(const QString   &word,
                             const Direction &direction) {
	SecureArray secure_array_word = direction == Decode
	                                ? hexToArray(word)
	                                : word.toUtf8();
    Cipher cipher(QLatin1String("aes256"), Cipher::CBC,
                  Cipher::DefaultPadding, direction, key, iv);
    SecureArray cipher_word = cipher.update(secure_array_word);
    if(!cipher.ok()) { qDebug() << "failed to update cipher for" << word; }
    SecureArray final = cipher.final();
    if(!cipher.ok()) { qDebug() << "failed to finalize cipher for" << word; }
    return direction == Decode ? QString("%1%2").arg(cipher_word.data())
                                                .arg(final.data())
                               : QString("%1%2").arg(arrayToHex(cipher_word.toByteArray()))
                                                .arg(arrayToHex(final.toByteArray()));
}

void Cipherize::cipherOfficialExampleTest(const QString &text) {
    SecureArray secureArrayToEncode = text.toUtf8();
    Cipher cipher(QLatin1String("aes256"),Cipher::CBC,
                  Cipher::DefaultPadding, Encode, key, iv);
    SecureArray secureArrayUpdated = cipher.update(secureArrayToEncode);
    if (!cipher.ok())
        qDebug() <<"Update failed";
    qDebug() << "AES256 encryption of" << secureArrayToEncode.data();
    qDebug() << "is [" << qPrintable(arrayToHex(secureArrayUpdated.toByteArray())) << "]";
    SecureArray secureArrayFinale = cipher.final();
    if (!cipher.ok())
        qDebug() << "Final failed";
    qDebug() << "Final block for AES256 encryption is"
             << "[0x" << qPrintable(arrayToHex(secureArrayFinale.toByteArray())) << "]";
    // DECODE MODE from secure array updated
    cipher.setup( Decode, key, iv );
    SecureArray cipherText = secureArrayUpdated.append(secureArrayFinale);
    SecureArray plainText = cipher.update(cipherText);
    if (!cipher.ok())
        qDebug() << "Update failed";
    qDebug() << "Decryption using AES256 of [0x"
             << qPrintable(arrayToHex(cipherText.toByteArray())) << "]"
             << "is" << plainText.data();
    plainText = cipher.final();
    if (!cipher.ok())
        qDebug() << "Final failed";
    qDebug() << "Final decryption block using AES256 is" << plainText.data();
    qDebug() << "One step decryption using AES256:"
             << SecureArray(cipher.process(cipherText)).data();
}

//********  XML / File => read/write  ****************************************

bool Cipherize::openFile(QFile *f) {
    if (!f->open(QIODevice::ReadWrite |
                 QIODevice::Text))      {
        qDebug() << "Failed to open/create file.";
        return false; }
    return true;
}

bool Cipherize::truncateFile(QFile *f) {
    if (!f->open(QIODevice::ReadWrite |
                 QIODevice::Truncate  |
                 QIODevice::Text)) {
        qDebug() << "Failed to truncate file.";
        return false; }
    return true;
}

void Cipherize::recordStremXML() {
    QTextStream fileStream(file);
    fileStream << doc->toString();
    file->close();
}

void Cipherize::loadXMLfile() {
    doc = new QDomDocument("XML_Cipher_AES_256");
    if (openFile(file)) {
        while (!doc->setContent(file)) {
            initXMLfile(); }
        file->close(); }
}

void Cipherize::initXMLfile() {
    file->close();
    if (truncateFile(file)) {
        QDomElement aesTag = doc->createElement("aes256");
        doc->appendChild(aesTag);
        QDomElement cipherData = doc->createElement("cipher");
        aesTag.appendChild(cipherData);
        recordStremXML(); }
    if(!openFile(file))
        qDebug() << "failed to open file at initXMLfile end.";
}

QString Cipherize::getCipherXML() {
    QDomNode doc_cipher = doc->elementsByTagName("aes256").at(0);
    QString result = doc_cipher.firstChildElement("cipher").text();
    return result;
}

bool Cipherize::setCipherXML(const QString &cipher) {
    QDomText encodedXML = doc->createTextNode(cipher);
    QDomElement cipherData = doc->elementsByTagName("cipher").at(0).toElement();
    if(cipherData.text().isEmpty()) {
        cipherData.appendChild(encodedXML); }
    else {
        cipherData.removeChild(cipherData.firstChild());
        cipherData.appendChild(encodedXML); }
    if (!truncateFile(file))
        return false;
    recordStremXML();
    return true;
}
